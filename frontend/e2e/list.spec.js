'use strict';

const HttpBackend = require('http-backend-proxy');

const proxy = new HttpBackend(browser);

describe('The product list view', function () {

  const defaultURL = 'https://s3-ap-southeast-1.amazonaws.com/' +
    'theincitement.com/exam/productfeed.xml';

  const emptyResultsURL = 'http://test.com/test.xml';

  beforeAll(function () {
    proxy.onLoad.reset();

    proxy.onLoad
      .whenPOST(/\/products\/parse.*/, {url: defaultURL})
      .respond(200, {
        products: [
          {
            productID: 1,
            name: 'test',
            description: 'test description',
            price: 100,
            currency: 'RUB',
            categories: [{category: 'test'}],
            productURL: 'http://test.test',
            imageURL: 'http://test.image'
          },
          {}
        ]
      });

    proxy.onLoad
      .whenPOST(/\/products\/parse.*/, {url: emptyResultsURL})
      .respond(200, {products: []});

    proxy.onLoad.whenGET(/.*/).passThrough();
  });

  beforeEach(function () {
    browser.get('/');
    browser.waitForAngular();
  });

  it('should display all products', function () {
    element(by.css('[type="submit"]')).click();
    expect(element(by.repeater('product in model.list')).isPresent())
      .toBe(true);
    const productsCount =
      element.all(by.repeater('product in model.list')).count();
    expect(productsCount).toEqual(2);
  });

  it('should display id of first product', function () {
    element(by.css('[type="submit"]')).click();

    const list = element.all(by.repeater('product in model.list'));

    expect(list.get(0).element(by.css('.panel-heading span')).getText())
      .toEqual('1');
  });

  it('should display name of first product', function () {
    element(by.css('[type="submit"]')).click();

    const list = element.all(by.repeater('product in model.list'));

    expect(list.get(0).element(by.css('.product-name')).getText())
      .toEqual('test');
  });

  it('should display description of first product', function () {
    element(by.css('[type="submit"]')).click();

    const list = element.all(by.repeater('product in model.list'));

    expect(list.get(0).element(by.css('.product-description')).getText())
      .toEqual('test description');
  });

  it('should display price of first product', function () {
    element(by.css('[type="submit"]')).click();

    const list = element.all(by.repeater('product in model.list'));

    expect(list.get(0).element(by.css('.product-price')).getText())
      .toEqual('100 RUB');
  });

  it('should display image of first product', function () {
    element(by.css('[type="submit"]')).click();

    const list = element.all(by.repeater('product in model.list'));

    expect(list.get(0).element(by.css('img')).getAttribute('src'))
      .toEqual('http://test.image/');
  });

  it('should display link of first product', function () {
    element(by.css('[type="submit"]')).click();

    const list = element.all(by.repeater('product in model.list'));

    expect(list.get(0).element(by.css('.product-link')).getAttribute('href'))
      .toEqual('http://test.test/');
  });

  it('should display categories of first product', function () {
    element(by.css('[type="submit"]')).click();

    const list = element.all(by.repeater('category in product.categories'));

    expect(list.count()).toEqual(1);
  });

  it('should display warning message if products not found', function () {
    element(by.css('[name="url"]')).clear();
    element(by.css('[name="url"]')).sendKeys(emptyResultsURL);
    element(by.css('[type="submit"]')).click();

    const emptyResultsMessageElement =
      element(by.css('product-list-component h2.text-warning'));

    expect(emptyResultsMessageElement.isPresent()).toBe(true);

    expect(emptyResultsMessageElement.getText()).toEqual('Products not found.');

    const list = element.all(by.repeater('product in model.list'));

    expect(list.count()).toEqual(0);
  });

});
