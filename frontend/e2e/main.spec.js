'use strict';

const HttpBackend = require('http-backend-proxy');

const proxy = new HttpBackend(browser);

describe('The main view', function () {

  const defaultURL = 'https://s3-ap-southeast-1.amazonaws.com/' +
    'theincitement.com/exam/productfeed.xml';

  const failURL = 'http://theincitement.com/test.xml';

  beforeAll(function () {
    proxy.onLoad.reset();

    proxy.onLoad
      .whenPOST(/\/products\/parse.*/, {url: defaultURL})
      .respond(200, {products: [{}]});

    proxy.onLoad
      .whenPOST(/\/products\/parse.*/, {url: failURL})
      .respond(500, {products: [{}]});

    proxy.onLoad.whenGET(/.*/).passThrough();
  });

  beforeEach(function () {
    browser.get('/');
    browser.waitForAngular();
  });

  it('should display default url in field', function () {
    expect(element(by.css('[name="url"]')).getAttribute('value'))
      .toEqual(defaultURL);
  });

  it('should display error if submited empty field', function () {
    element(by.css('[name="url"]')).clear();
    element(by.css('[type="submit"]')).click();

    expect(element(by.css('[name="url"] + span.text-danger')).isPresent())
      .toBe(true);

    expect(element(by.css('[name="url"] + span.text-danger')).getText())
      .toEqual('Is required field.');

    expect(element(by.css('.form-group.has-error')).isPresent())
      .toBe(true);
  });

  it('should display error if url invalid', function () {
    element(by.css('[name="url"]')).clear();
    element(by.css('[name="url"]')).sendKeys('http://test');
    element(by.css('[type="submit"]')).click();

    expect(element(by.css('[name="url"] + span.text-danger')).isPresent())
      .toBe(true);

    expect(element(by.css('[name="url"] + span.text-danger')).getText())
      .toEqual('Invalid URL.');

    expect(element(by.css('.form-group.has-error')).isPresent())
      .toBe(true);
  });

  it('should display products count', function () {
    element(by.css('[type="submit"]')).click();
    expect(element(by.css('product-list-component h2')).getText())
      .toEqual('1 products');
  });

  it('should display error if request failled', function () {
    element(by.css('[name="url"]')).clear();
    element(by.css('[name="url"]')).sendKeys(failURL);
    element(by.css('[type="submit"]')).click();

    expect(element(by.css('.form-error p.text-danger')).isPresent())
      .toBe(true);

    expect(element(by.css('.form-error p.text-danger')).getText())
      .toEqual('Fail request. Please, try again later.');
  });

});
