'use strict';

const paths = {
  'src': 'src',
  'dist': 'dist',
  'e2e': 'e2e',
  'tmp': '.tmp'
};

exports.config = {
  capabilities: {
    'browserName': 'chrome'
  },

  baseUrl: 'http://localhost:3000',

  specs: [paths.e2e + '/**/*.js'],

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000
  }
};
