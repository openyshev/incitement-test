const XmlUrlValidator = function () {
  return {
    require: 'ngModel',
    restrict: 'A',
    link: function (scope, elm, attrs, ctrl) {
      ctrl.$validators['xml-url-validator'] = function (modelValue, viewValue) {
        const XML_URL_REGEXP = /^(http(s?):\/\/)?(((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|([a-zA-Z0-9\-\_]+(\.[a-zA-Z]{2,3})))+(\/[a-zA-Z0-9\_\-\s\.\/\?\%\#\&\=]*)?\.xml$/;

        if (ctrl.$isEmpty(modelValue)) {
          return true;
        }

        if (XML_URL_REGEXP.test(viewValue)) {
          return true;
        }

        return false;
      };
    }
  };
};

export default XmlUrlValidator;
