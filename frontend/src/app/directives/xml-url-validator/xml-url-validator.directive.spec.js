describe('Unit: XmlUrlValidator', function () {

  let form;
  let $scope;

  beforeEach(angular.mock.module('incitement'));

  beforeEach(inject(function ($compile, $rootScope) {
    $scope = $rootScope.$new();
    const element = angular.element(`
      <form name="form">
        <input name="url" type="url" ng-model="model.url" xml-url-validator />
      </form>`
    );

    $compile(element)($scope);

    form = $scope.form;
  }));

  it('form should be invalid', function () {
    $scope.model = {url: 'http://test.test'};
    $scope.$digest();
    expect(form.url.$valid).toBe(false);
  });

  it('form should be valid', function () {
    $scope.model = {url: 'http://test.com/test.xml'};
    $scope.$digest();
    expect(form.url.$valid).toBe(true);
  });
});
