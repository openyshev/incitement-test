describe('Unit: productService', () => {

  let service;
  let http;
  let q;

  beforeEach(angular.mock.module('incitement'));

  beforeEach(angular.mock.inject(($q, $http, _productService_) => {
    q = $q;
    http = $http;
    service = _productService_;
  }));

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(service.parseProducts).toBeDefined();
  });

  it('should call api', () => {
    const httpDeffer = q.defer();
    const url = 'http://test.com/test.xml';
    service.apiURL = 'http://test';

    spyOn(http, 'post').and.returnValue(httpDeffer.promise);
    service.parseProducts(url);

    httpDeffer.resolve();

    expect(http.post).toHaveBeenCalledWith(
      service.apiURL + '/products/parse',
      Object({url: url})
    );
    
    expect(service.parseProducts(url)).toEqual(httpDeffer.promise);
  });
});
