function ProductService($http, appParams) {
  'ngInject';

  const service = {};
  service.apiURL = appParams.apiURL;

  service.parseProducts = (productsURL) => {
    const promise =
      $http.post(service.apiURL + '/products/parse', {url: productsURL});
    return promise;
  };

  return service;
}

export default ProductService;
