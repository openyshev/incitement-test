export default function providers($logProvider, $locationProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);
  $logProvider.debugEnabled(true);

}
