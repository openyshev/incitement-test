export default function routes($routeProvider) {
  'ngInject';

  $routeProvider
    .when('/', {
      template: '<main-component></main-component>'
    })
    .otherwise({
      redirectTo: '/'
    });

}
