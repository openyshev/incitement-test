import params from './config/params';
import providers from './config/providers';
import productService from './services/product/product.service';
import routes from './config/routes';
import run from './config/run';
import MainComponent from './components/main/main.component';
import ProductListComponent from './components/product-list/product-list.component';
import XmlUrlValidator from './directives/xml-url-validator/xml-url-validator.directive';

angular.module('incitement', ['ngRoute'])
  .config(providers)
  .config(routes)
  .constant('appParams', params)
  .run(run)
  .service('productService', productService)
  .component('mainComponent', MainComponent)
  .component('productListComponent', ProductListComponent)
  .directive('xmlUrlValidator', XmlUrlValidator);
