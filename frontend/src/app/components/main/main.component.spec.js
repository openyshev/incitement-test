describe('Unit: mainComponent', () => {

  let componentCtrl;
  let deferred;
  let productService;
  let scope;

  beforeEach(angular.mock.module('incitement'));

  beforeEach(angular.mock.inject(($rootScope, $q, $componentController, _productService_) => {
    scope = $rootScope.$new();
    productService = _productService_;
    deferred = $q.defer();
    componentCtrl = $componentController('mainComponent', {
      $scope: scope,
      productService: productService
    });
  }));

  it('should be defined', () => {
    expect(componentCtrl).toBeDefined();
  });

  it('should set defaults on controller initialization', () => {
    componentCtrl.$onInit();
    expect(componentCtrl.formError).toBe(false);
    expect(componentCtrl.formLoading).toBe(false);
    expect(componentCtrl.formData).toEqual({
      url: 'https://s3-ap-southeast-1.amazonaws.com/' +
        'theincitement.com/exam/productfeed.xml'
    });
    expect(componentCtrl.productList).toBe(null);
  });

  it('should not send form if form-data invalid', () => {
    spyOn(productService, 'parseProducts');
    componentCtrl.sendForm({$invalid: true});
    expect(productService.parseProducts).not.toHaveBeenCalled();
  });

  it('should send form if form-data valid', () => {
    spyOn(productService, 'parseProducts').and.returnValue(deferred.promise);
    componentCtrl.formData = {url: 'test_url'};
    const form = {
      $invalid: false,
      $setPristine: function () {
        return;
      }
    };
    const response = {
      data: {products: []}
    };
    componentCtrl.sendForm(form);
    deferred.resolve(response);
    scope.$root.$digest();
    expect(productService.parseProducts).toHaveBeenCalled();
  });

  it('should display error if request failed', () => {
    spyOn(productService, 'parseProducts').and.returnValue(deferred.promise);
    componentCtrl.formData = {url: 'test_url'};
    const form = {
      $invalid: false,
      $setPristine: function () {
        return;
      }
    };
    componentCtrl.sendForm(form);
    deferred.reject();
    scope.$root.$digest();
    expect(productService.parseProducts).toHaveBeenCalled();
    expect(componentCtrl.formError).toBe(true);
  });

});
