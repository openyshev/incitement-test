const MainComponent = {
  templateUrl: 'app/components/main/main.html',
  controllerAs: 'model',
  controller: function ($log, productService) {
    'ngInject';

    const model = this;

    model.$onInit = () => {
      $log.debug('main-component was initialized..');
      model.formError = false;
      model.formLoading = false;
      model.formData = {
        url: 'https://s3-ap-southeast-1.amazonaws.com/' +
          'theincitement.com/exam/productfeed.xml'
      };
      model.productList = null;
    };

    model.sendForm = (form) => {
      if (form.$invalid) {
        return;
      }

      $log.debug('form was sended..');

      model.productList = null;
      model.formLoading = true;
      model.formError = false;

      productService.parseProducts(model.formData.url)
        .then(
          function (response) {
            form.$setPristine();
            model.formLoading = false;
            model.productList = response.data.products;
          },
          function () {
            model.formLoading = false;
            model.formError = true;
          });
    };
  }
};

export default MainComponent;
