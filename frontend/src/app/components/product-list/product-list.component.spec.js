describe('Unit: productListComponent', () => {

  let componentCtrl;
  let scope;

  beforeEach(angular.mock.module('incitement'));

  beforeEach(angular.mock.inject(($rootScope, $componentController) => {
    scope = $rootScope.$new();
    componentCtrl = $componentController('productListComponent', {
      $scope: scope
    });
  }));

  it('should be defined', () => {
    expect(componentCtrl).toBeDefined();
  });

});
