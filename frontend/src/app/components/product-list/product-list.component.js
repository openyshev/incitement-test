const ProductListComponent = {
  templateUrl: 'app/components/product-list/product-list.html',
  controllerAs: 'model',
  bindings: {
    list: '='
  },
  controller: function ($log) {
    'ngInject';

    const model = this;

    model.$onInit = () => {
      $log.debug('product-list-component was initialized..');
    };
  }
};

export default ProductListComponent;
