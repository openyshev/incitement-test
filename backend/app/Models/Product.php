<?php

namespace App\Models;

use App\Helpers\XMLHelper as XMLHelper;

class Product {

  public static function parseFromURL($url) {
    if (!XMLHelper::isXmlStructureValid($url)) {
      return [];
    }

    $products = simplexml_load_file($url);

    $result = [];
    foreach ($products->product as $product) {
      $productCurrency = (string) $product->price->attributes()['currency'];
      $productDescription = (string) $product->description;
      $product = (array) $product;
      $product['currency'] = $productCurrency;
      $product['description'] = $productDescription;
      $product['categories'] = (array) $product['categories'];
      unset($product['additional']);
      $result[] = $product;
    }

    return $result;
  }


}
