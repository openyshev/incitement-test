<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Models\Product as Product;

class ProductsController extends Controller
{
    public function parse(Request $request) {
      $url = $request->url;

      if (empty($url))
        return;

      $products = Product::parseFromURL($url);

      return response()->json(['products' => $products]);
    }
}
