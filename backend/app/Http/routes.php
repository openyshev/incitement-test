<?php

Route::match(
  ['post', 'options'], '/products/parse', 'ProductsController@parse'
)->middleware('cors');
