<?php

namespace App\Helpers;

class XMLHelper {

  public static function isXmlStructureValid($file) {
    $xmlContent = file_get_contents($file);
    libxml_use_internal_errors(true);
    $doc = new \DOMDocument('1.0', 'utf-8');
    $doc->loadXML($xmlContent);
    $errors = libxml_get_errors();
    libxml_clear_errors();
    return empty($errors);
  }

}
