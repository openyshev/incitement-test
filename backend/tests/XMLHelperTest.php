<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Helpers\XMLHelper as XMLHelper;

class XMLHelperTest extends TestCase
{

    public function testShouldReturnErrors() {
      $url = base_path() . '/tests/invalid_productfeed.xml';
      $valid = XMLHelper::isXmlStructureValid($url);
      $this->assertEquals(false, $valid);
    }

    public function testShouldNotReturnErrors() {
      $url = base_path() . '/tests/productfeed.xml';
      $valid = XMLHelper::isXmlStructureValid($url);
      $this->assertEquals(true, $valid);
    }

}
