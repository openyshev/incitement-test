<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Product as Product;

class ProductTest extends TestCase
{

    public function testShouldParseValidXML() {
      $url = base_path() . '/tests/productfeed.xml';
      $result = Product::parseFromURL($url);
      $this->assertCount(2, $result);
    }

    public function testShouldParseInValidXML() {
      $url = base_path() . '/tests/invalid_productfeed.xml';
      $result = Product::parseFromURL($url);
      $this->assertCount(0, $result);
    }

    public function testShouldReturnValidData() {
      $url = base_path() . '/tests/productfeed.xml';
      $result = Product::parseFromURL($url);
      $this->assertEquals('1', $result[0]['productID']);
      $this->assertEquals('name', $result[0]['name']);
      $this->assertEquals('100', $result[0]['price']);
      $this->assertEquals('RUB', $result[0]['currency']);
      $this->assertEquals('description', $result[0]['description']);
      $this->assertEquals('http://image.test', $result[0]['imageURL']);
      $this->assertEquals('http://product.test', $result[0]['productURL']);
      $this->assertCount(1, $result[0]['categories']);
    }
}
