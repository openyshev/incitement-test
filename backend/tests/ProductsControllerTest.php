<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\ProductsController as ProductsController;

class ProductsControllerTest extends TestCase
{

    public function testShouldReturnProducts() {
      $url = base_path() . '/tests/productfeed.xml';
      $controller = new ProductsController();
      $request = new Request();
      $request->url = $url;
      $result =$controller->parse($request);
      $products = json_decode($result->content(), true);
      $this->assertArrayHasKey('products', $products);
      $this->assertCount(2, $products['products']);
    }

    public function testShouldReturnEmptyProducts() {
      $url = base_path() . '/tests/invalid_productfeed.xml';
      $controller = new ProductsController();
      $request = new Request();
      $request->url = $url;
      $result =$controller->parse($request);
      $products = json_decode($result->content(), true);
      $this->assertArrayHasKey('products', $products);
      $this->assertCount(0, $products['products']);
    }

}
