## Incitement Assessment Test ##
### Products Parser ###

> Note: application prepared only for development stage

**Developer: Roman Openyshev**


----------

 **Technologies and tools:**

 - *backend*: laravel 5.2, composer, phpunit;
 - *frontend*: angular 1.5, es2015, gulp, webpack, bower, npm, bootstrap, karma, protractor, jasmine.

----------

**Build and run**

Backend:

    cd ./backend
    composer install
    php artisan serve

Frontend:


    cd ./frontend
    npm install
    bower install
    gulp serve

> Note: frontend running on http://localhost:3000/, backend - http://localhost:8000/


----------
**Testing**

Backend:

    ./backend
    phpunit

 Frontend unit tests:

    ./frontend
    gulp test

 Frontend e2e tests:


    ./frontend
    gulp protractor
